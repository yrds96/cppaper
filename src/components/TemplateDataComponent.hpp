#ifndef TEMPLATE_DATA_COMPONENT_INCLUDED
#define TEMPLATE_DATA_COMPONENT_INCLUDED

#include "inja/inja.hpp"

namespace cppaper {
struct TemplateDataComponent {
  inja::json templateData;
};
}

#endif //TEMPLATE_DATA_COMPONENT_INCLUDED
