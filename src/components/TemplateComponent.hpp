#ifndef TEMPLATE_COMPONENT_INCLUDED
#define TEMPLATE_COMPONENT_INCLUDED

#include "inja/inja.hpp"

namespace cppaper {
struct TemplateComponent {
  inja::Template template;
};
}

#endif //Template_INCLUDED
